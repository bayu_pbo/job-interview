# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait)

Penggunaan matematika dan algoritma pemrograman dalam kodingan di bawah memungkinkan untuk menyelesaikan masalah menghitung kapasitas penyimpanan yang dibutuhkan dengan cara yang efektif dan efisien.

Dalam Usecase Penyimpanan dimana dapat menghitung kapasitas penyimpanan


```
class File extends Storage {
    private String name;
    private String type;
    private double size;


    public File(String name, String type, double size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public String toString() {
        return "Nama File: " + name + "\nTipe File: " + type + "\nFile size: " + size + "KB\n";
    }

// Implementasi method calculateStorageCapacity() dari kelas Storage

    @Override
    double calculateStorageCapacity() {
        return size;
    }
}
```


`calculateStorageCapacity()` yang merupakan bagian dari kelas abstrak Storage.

Pada kelas File, kapasitas penyimpanan dapat dihitung dengan mudah yaitu hanya dengan mengambil nilai atribut **size**. 

Sedangkan pada kelas Folder, kapasitas penyimpanan dihitung dengan menjumlahkan ukuran semua file yang ada di dalamnya. Hal ini dilakukan dengan mengakses semua file yang ada di dalam array files dan menjumlahkan ukuran masing-masing file menggunakan perulangan **for**.

```
class Folder extends Storage {
    private String name;
    private ArrayList<File> files; 

    public Folder(String name) {
        this.name = name;
        this.files = new ArrayList<File>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<File> getFiles() {
        return files;
    }

    public void addFile(File file) {
        this.files.add(file);
    }

    public void removeFile(File file) {
        this.files.remove(file);
    }

    public String toString() {
        String output = "Nama Folder : " + name + "\n";//
        for (File file : files) {
            output += file.toString();
        }
        return output;
    }

// Implementasi method calculateStorageCapacity() dari kelas Storage

    @Override
    double calculateStorageCapacity() {
        double totalSize = 0;
        for (File file : files) {
            totalSize += file.getSize();
        }
        return totalSize;
    }
}

 public class Mainku {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String username = "bayu";
        String password = "bayu123";
        
      boolean isValid = false; // Variabel untuk menyimpan status validitas username dan password

do {
    System.out.print("Masukkan Nama Pengguna: ");
    String usernameInput = sc.nextLine();
    System.out.print("Masukkan Kata Sandi: ");
    String passwordInput = sc.nextLine();

    if (usernameInput.equals(username) && passwordInput.equals(password)) {
        isValid = true; // Set isValid menjadi true jika username dan password sesuai
    } else {
        System.out.println("Username atau password tidak valid! Silakan coba lagi."); // Pesan kesalahan jika username atau password tidak valid
    }
    } while (!isValid); // Melakukan loop selama username dan password tidak valid

    Folder folder = new Folder("File Ku");

    // Menampilkan Pilihan Menu dan akan looping sampai user memilih untuk exit
    boolean isRunning = true;
        while (isRunning) {
            System.out.println("===Program Sederhana Sebuah Gdrive===");
            System.out.println("Selamat datang, " + username + "!");
            System.out.println("\nMenu:");
            System.out.println("1. Upload File");
            System.out.println("2. Edit Type File");
            System.out.println("3. Hapus File");
            System.out.println("4. Tampilkan File");
            System.out.println("5. Ganti Nama Folder");
            System.out.println("6. Ganti Nama File");
            System.out.println("7. Bagikan File");
            System.out.println("8. Exit");

            System.out.print("Pilih Menu: ");
            int menu = sc.nextInt();
            sc.nextLine();

            switch (menu) {
                case 1:
                    // membuat file baru dan akan disimpan ke dalam folder
                    System.out.print("\nMasukan Nama File: ");
                    String fileName = sc.nextLine();

                    System.out.print("Masukan Jenis File (PDF/WORD/Excel/): ");
                    String fileType = sc.nextLine();

                    System.out.print("Masukkan Ukuran File (KB): ");
                    double fileSize = sc.nextDouble();
                    File newFile = new File(fileName, fileType, fileSize);
                    folder.addFile(newFile);
                    System.out.println("\nFile berhasil diupload!");
                    break;
            
               case 2:
                // menu untuk mengedit jenis file 
                System.out.print("\nMasukan Nama File: ");
                fileName = sc.nextLine();

                System.out.print("Masukan Jenis File Baru (PDF/WORD): ");
                fileType = sc.nextLine();

                for (File file : folder.getFiles()) {
                if (file.getName().equals(fileName)) {
                file.setType(fileType);
                System.out.println("Jenis file berhasil diubah!");
                break;
        
                    }
                }
                break;

                case 3:
                // Menghapus File
                System.out.print("\nMasukan Nama File: ");
                fileName = sc.nextLine();

                for (File file : folder.getFiles()) {
                    if (file.getName().equals(fileName)) {
                        folder.removeFile(file);
                        System.out.println("File berhasil dihapus!");
                        break;
                    }
                }
                break;
                
                case 4:
                    // menampilkan semua file yang ada di folder
                    System.out.println("\nIsi folder " + folder.getName() + ":");
                    System.out.println(folder.toString());
                    System.out.println("Penyimpanan Terpakai: " + folder.calculateStorageCapacity() + " KB");
                    break;
                
                case 5:
                // merubah nama folder
                System.out.print("\nMasukan Nama Folder Baru: ");
                String folderName = sc.nextLine();
                folder.setName(folderName);
                System.out.println("Nama folder berhasil diubah!");
                break;
                
                case 6:
                // Merubah nama file
                System.out.print("\nMasukan Nama File: ");
                fileName = sc.nextLine();

                System.out.print("Masukan Nama File Baru: ");
                String newFileName = sc.nextLine();

                for (File file : folder.getFiles()) {
                    if (file.getName().equals(fileName)) {
                        file.setName(newFileName);
                        System.out.println("Nama file berhasil diubah!");
                        break;
                    }
                }
                break;
                
                case 7:
    // membagikan file
    System.out.print("\nMasukan Nama File: ");
    fileName = sc.nextLine();

    for (File file : folder.getFiles()) {
        if (file.getName().equals(fileName)) {
            boolean isEmailValid = false;
            String email;

            do {
                System.out.print("Masukkan Email Penerima: ");
                email = sc.nextLine();

                if (email.contains("@")) {
                    isEmailValid = true;
                    file.setType(fileName);
                    System.out.println("File berhasil dibagikan ke " + email + "!");
                } else {
                    System.out.println("Alamat email tidak valid! Silakan coba lagi.");
                }
            } while (!isEmailValid);

            break;
        }
    }
    break;
                
                case 8:
                // Keluar Program
                System.out.println("Terima kasih telah menggunakan program ini!");
                isRunning = false;
                break;
                default:
                System.out.println("Menu yang dipilih tidak valid!");
                break;
        }
    }
}
} 


```




# 2. Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

Penggunaan algoritma pada kodingan di bawah memungkinkan untuk menghitung kapasitas penyimpanan yang dibutuhkan dengan cara yang efektif dan efisien.

1. Buat kelas abstrak Storage yang memiliki method abstract `calculateStorageCapacity()` untuk menghitung kapasitas penyimpanan.

```
import java.util.ArrayList;
import java.util.Scanner;

// Deklarasi kelas Storage sebagai kelas abstrak
abstract class Storage {
    // Deklarasi method abstract calculateStorageCapacity()
    abstract double calculateStorageCapacity();
}
```
2. Buat kelas **File** yang merupakan turunan dari kelas Storage. Kelas ini memiliki atribut `name`, `type`, dan `size`, serta konstruktor dan method getter dan setter untuk masing-masing atribut. Kelas ini juga memiliki method `toString()` untuk mencetak informasi file dan mengimplementasikan method `calculateStorageCapacity()` dari kelas Storage.

```
class File extends Storage {
    private String name;
    private String type;
    private double size;

// Deklarasi konstruktor dengan tiga parameter
    public File(String name, String type, double size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }
// Deklarasi method getter untuk atribut name
    public String getName() {
        return name;
    }
// Deklarasi method setter untuk atribut name
    public void setName(String name) {
        this.name = name;
    }
// Deklarasi method getter untuk atribut type
    public String getType() {
        return type;
    }
// Deklarasi method setter untuk atribut type
    public void setType(String type) {
        this.type = type;
    }
// Deklarasi method setter untuk atribut size
    public double getSize() {
        return size;
    }
// Deklarasi method setter untuk atribut size
    public void setSize(double size) {
        this.size = size;
    }
// Deklarasi method toString() untuk mencetak informasi file
    public String toString() {
        return "Nama File: " + name + "\nTipe File: " + type + "\nFile size: " + size + "KB\n";
    }
// Implementasi method calculateStorageCapacity() dari kelas Storage
    @Override
    double calculateStorageCapacity() {
        return size;
    }
}
```
3. Buat kelas Folder yang juga merupakan turunan dari kelas Storage. Kelas ini memiliki atribut name dan files (berupa ArrayList dari File), serta konstruktor dan method getter dan setter untuk masing-masing atribut. Kelas ini juga memiliki method `addFile()` dan `removeFile() `untuk menambahkan dan menghapus file dari folder, method `toString()` untuk mencetak informasi folder dan file-file yang ada di dalamnya, serta mengimplementasikan method `calculateStorageCapacity()` dari kelas Storage.

```
class Folder extends Storage {
    private String name;
    private ArrayList<File> files; // Deklarasi atribut name dan files (berupa ArrayList<File>)
// Deklarasi konstruktor dengan satu parameter
    public Folder(String name) {
        this.name = name;
        this.files = new ArrayList<File>();
    }
// Deklarasi method getter untuk atribut name
    public String getName() {
        return name;
    }
// Deklarasi method setter untuk atribut name
    public void setName(String name) {
        this.name = name;
    }
// Deklarasi method getter untuk atribut files
    public ArrayList<File> getFiles() {
        return files;
    }
// Deklarasi method untuk menambahkan file ke dalam folder
    public void addFile(File file) {
        this.files.add(file);
    }
// Deklarasi method untuk menghapus file dari folder
    public void removeFile(File file) {
        this.files.remove(file);
    }
// Deklarasi method toString() untuk mencetak informasi folder dan file-file yang ada di dalamnya
    public String toString() {
        String output = "Nama Folder : " + name + "\n";// Inisialisasi variabel output dengan nama folder
        for (File file : files) {
            output += file.toString();
        }
        return output;
    }
// Implementasi method calculateStorageCapacity() dari kelas Storage
    @Override
    double calculateStorageCapacity() {
        double totalSize = 0;
        for (File file : files) {
            totalSize += file.getSize();
        }
        return totalSize;
    }
}
```
4. Pada kelas File, kapasitas penyimpanan dihitung dengan mengambil nilai atribut `size`.

5. Pada kelas Folder, kapasitas penyimpanan dihitung dengan menjumlahkan ukuran semua file yang ada di dalamnya. Hal ini dilakukan dengan mengakses semua file yang ada di dalam array files dan menjumlahkan ukuran masing-masing file menggunakan perulangan for.

# 3. Mampu menjelaskan konsep dasar OOP
Konsep dasar OOP (Object-Oriented Programming) adalah sebuah pemrograman yang fokus pada objek atau entitas dalam program yang memiliki atribut dan perilaku tertentu.

Objek dalam OOP memiliki beberapa karakteristik (4 pilar) seperti:

- Encapsulation: Kemampuan untuk menyembunyikan data dan implementasi dalam sebuah objek dan hanya memberikan akses melalui metode atau antarmuka yang didefinisikan.

- Inheritance: Kemampuan untuk membuat objek baru yang memiliki karakteristik dari objek yang sudah ada, sehingga memungkinkan penggunaan kembali kode dan mempermudah pengorganisasian kode.

- Polymorphism: Kemampuan untuk memungkinkan objek untuk memiliki banyak bentuk atau perilaku yang berbeda tergantung pada konteks penggunaannya.

- Abstraction: kemampuan untuk mengisolasi detail yang tidak diperlukan dan hanya mengekspos informasi yang relevan atau penting.


# 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

class File extends Storage {

    private String name;
    private String type;
    private double size;

access modifier private, atribut `name`, `type`, dan `size` hanya bisa diakses oleh method di dalam kelas File.
dengan menggunakan method getter dan setter yang memiliki access modifier `public`, kita dapat mengakses dan mengubah nilai dari atribut tersebut dari luar kelas File

```

public File(String name, String type, double size) {
        this.name = name;
        this.type = type;
        this.size = size;

         public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
```

    

Hal yang sama juga terjadi pada kelas Folder, dimana terdapat dua atribut private yaitu name dan files. Kemudian, terdapat empat method public yaitu getter dan setter untuk atribut name, method `getFiles()` untuk mengambil list file, serta method `addFile()` dan `removeFile()` untuk menambahkan dan menghapus file dari list.



    class Folder extends Storage {
    private String name;
    private ArrayList<File> files;
     public Folder(String name) {
        this.name = name;
        this.files = new ArrayList<File>();
       
       
```
       }
public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<File> getFiles() {
        return files;
    }

    public void addFile(File file) {
        this.files.add(file);
    }

    public void removeFile(File file) {
        
    public String toString() {
        String output = "Nama Folder : " + name + "\n";/
        for (File file : files) {
            output += file.toString();
        }
        return output;
          }
```
Dengan menggunakan access modifier **private** dan **public** serta getter dan setter, kita dapat mengontrol dan membatasi akses ke atribut dan method di dalam kelas File dan Folder. Ini adalah salah satu contoh penggunaan encapsulation pada Java.
  
# 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)
```
import java.util.ArrayList;
import java.util.Scanner;


abstract class Storage {
   calculateStorageCapacity()
    abstract double calculateStorageCapacity();
}
```
Pada kodingan di atas, penggunaan abstraction terlihat pada penggunaan kelas abstrak Storage yang memiliki method abstract `calculateStorageCapacity()`. Method ini harus diimplementasikan di kelas turunan yaitu kelas File dan Folder.

Kelas turunan yaitu kelas File dan Folder, harus mengimplementasikan method `calculateStorageCapacity()` dari kelas Storage. Hal ini menjadikan kelas File dan Folder memiliki implementasi yang berbeda untuk method `calculateStorageCapacity()`, tergantung dari karakteristik dari masing-masing kelas.

```
class File extends Storage {
    private String name;
    private String type;
    private double size;


    public File(String name, String type, double size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public String toString() {
        return "Nama File: " + name + "\nTipe File: " + type + "\nFile size: " + size + "KB\n";
    }

// Implementasi method calculateStorageCapacity() dari kelas Storage

    @Override
    double calculateStorageCapacity() {
        return size;
    }
```
Dengan menggunakan kelas abstrak dan method abstract, kita dapat mengabstraksi fungsionalitas dan memastikan bahwa kelas turunan mengimplementasikan method-method yang diperlukan dengan cara yang sesuai. Ini adalah salah satu contoh penggunaan abstraction 

# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

- Inheritance

```
class File extends Storage {
    private String name;
    private String type;
    private double size;
```
```
class Folder extends Storage {
    private String name;
    private ArrayList<File> files; 

    public Folder(String name) {
        this.name = name;
        this.files = new ArrayList<File>();
    }
```
Pada kodingan di atas, penggunaan Inheritance terlihat pada deklarasi kelas turunan yaitu kelas File dan Folder dengan menggunakan keyword "**extends**", untuk mewarisi sifat-sifat dari kelas induk yaitu kelas abstrak Storage. Dengan demikian, kelas turunan memiliki akses ke atribut dan method yang ada di kelas induknya.


- Polymorphism 

Penggunaan Polymorphism terlihat pada penggunaan  penggunaan method `override` pada kelas File dan Folder. Method `calculateStorageCapacity()` pada kelas Storage merupakan method abstract yang harus diimplementasikan di kelas turunan, yaitu kelas File dan Folder.

```
class File extends Storage {
    private String name;
    private String type;
    private double size;


    public File(String name, String type, double size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public String toString() {
        return "Nama File: " + name + "\nTipe File: " + type + "\nFile size: " + size + "KB\n";
    }

    @Override
    double calculateStorageCapacity() {
        return size;
    }
}
```

```
class Folder extends Storage {
    private String name;
    private ArrayList<File> files; // Deklarasi atribut name dan files (berupa ArrayList<File>)

    public Folder(String name) {
        this.name = name;
        this.files = new ArrayList<File>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<File> getFiles() {
        return files;
    }

    public void addFile(File file) {
        this.files.add(file);
    }

    public void removeFile(File file) {
        this.files.remove(file);
    }

    public String toString() {
        String output = "Nama Folder : " + name + "\n";/
        for (File file : files) {
            output += file.toString();
        }
        return output;
    }

// Implementasi method calculateStorageCapacity() dari kelas Storage
    @Override
    double calculateStorageCapacity() {
        double totalSize = 0;
        for (File file : files) {
            totalSize += file.getSize();
        }
        return totalSize;
    }
}
```


Kelas File dan Folder mengoverride method `calculateStorageCapacity()` dari kelas Storage dengan implementasi masing-masing. Dengan menggunakan konsep polymorphism, kita dapat memperlakukan objek dari kelas turunan sebagai objek dari kelas induk, sehingga memudahkan dalam memanipulasi objek-objek tersebut secara umum.

# 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Langkah-langkah untuk mendeskripsikan proses bisnis ke dalam OOP yaitu:

- Identifikasi objek-objek yang terlibat:

Dalam setiap use case, terdapat objek-objek yang terlibat dalam proses bisnis. Identifikasi objek-objek tersebut berdasarkan fitur-fitur yang ada dalam setiap use case.

- Tentukan kelas-kelas:

Setelah mengidentifikasi objek-objek, tentukan kelas-kelas yang mewakili objek-objek tersebut. Setiap kelas akan memiliki atribut dan metode yang sesuai dengan fungsionalitasnya.

- Definisikan atribut dan metode:

Untuk setiap kelas, definisikan atribut-atribut yang diperlukan untuk menyimpan informasi yang relevan. Misalnya, untuk kelas Folder, atribut dapat berupa nama folder, daftar file, dan kapasitas penyimpanan.

Selanjutnya, definisikan metode-metode yang akan mengimplementasikan fungsionalitas yang terkait dengan setiap use case. Misalnya, untuk kelas Folder, metode-metode dapat berupa UploadFile(), hapusFile(), tampilkanDaftarFile(), dan sebagainya.

- Hubungkan antara kelas:

Identifikasi hubungan dan keterkaitan antara kelas-kelas yang ada. Misalnya, kelas File dapat terhubung dengan kelas Folder melalui asosiasi atau komposisi, di mana Folder memiliki objek-objek File yang ada di dalamnya.

- Implementasikan logika:

Implementasikan logika yang terkait dengan setiap use case dalam metode-metode yang telah didefinisikan dalam kelas-kelas. Logika bisnis ini harus memastikan bahwa fungsionalitas yang diinginkan dalam setiap use case tercapai.

- Tes dan validasi:

Setelah implementasi selesai, lakukan pengujian dan validasi untuk memastikan bahwa fungsionalitas yang diharapkan berjalan dengan baik dan sesuai dengan kebutuhan bisnis.


# 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)

>Class diagram Menggunakan Mermaid
```mermaid
classDiagram
    class GoogleDrive {
        +listFiles()
        +uploadFile()
        +downloadFile()
        +shareFile()
    }

    class User {
        -name: string
        -email: string
        +login()
        +logout()
        +uploadFile()
        +downloadFile()
        +shareFile()
    }

    class File {
        -name: string
        -size: int
    }

    GoogleDrive --> User : Manages
    GoogleDrive --> File : Contains

```


>Class diagram

![Dokumentasi](Dokumentasi/USD.png)

> Usecase table


| Usecase Google Drive  | Nilai |
| ------ | ------ |
|     1.Membuat Folder:    |    90    |
|   2. Berbagi Folder dan File:     |     80   |
| 3. Mengelola Akses: | 75|
| 4. Mengunggah  Mengedit dan menghapus File: |100 |
| 5. Sinkronisasi Offline: |65 |
|6.Penyimpanan |70 |
|7.Singkronasi dengan aplikasi lain |70 |
|8. Komentar dan Anotasi |70 |
|9.Menghapus folder |60 |
|10. Pembaruan Otomatis:  | 60|
|11.Beli Penyimpanan | 50|
|12.Membuat Link Berbagi |50 |
|13.Beralih akun atau tambahkan akun |50 |
|14. Notifikasi |50 |
|15.Keep (Catatan) |50 |
|16. Riwayat Revisi |55 |
|17.Menyimpan File Ke Format lain |45 |
|18. Penyimpanan Aman |45 |
|19. menampilkan file di dalan folder  |40 |
|20. Membuat Salinan File |40 |
| 21.Menandai File dan Folder| 40|
|22.Penulusuran Lanjutan|40|
|23.Pencarian|35|
|24.Terbaru|30|
|25.Kalender|30|
|26.Megunduh File|30|
|27.Integrasi Aplikasi|30|
|28.Mengarsipkan File|30|
|29.Kontak|25|
|30.Menandai Bintang|25|
|31.Tampilan Berdasarkan Grid|25|
|32.Ubah Warna Folder|25|
|33.Mengunduh Folder|20|
|34.Spam|10|
|35.Task|10|
|36.Pemulihan Data|15|
|37.Penjadwalan Sinkronisasi|10|
|38.integrasi dengan Google Photos|5|
|39.Bantuan dan Dukungan|1|


# 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

> https://youtu.be/M7tv6MfmKdk

# 10. Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)
1. Menu Upload file

![Dokumentasi](Dokumentasi/1.jpg)

2. Menu Tampilkan

![Dokumentasi](Dokumentasi/2.jpg)

3. Menu ubah nama folder

![Dokumentasi](Dokumentasi/3.jpg)

4. Menu ubah nama file

![Dokumentasi](Dokumentasi/4.jpg)

5. Menu ubah jenis file

![Dokumentasi](Dokumentasi/5.jpg)

6. Menu bagikan

![Dokumentasi](Dokumentasi/6.jpg)

7. Menu Hapus File

![Dokumentasi](Dokumentasi/7.jpg)
