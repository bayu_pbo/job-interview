# 2. Mampu mendemonstrasikan pembuatan Docker Image melalui Dockerfile, sebagai bentuk implementasi dari sistem operasi.

Untuk membuat Docker Image menggunakan Dockerfile bisa sebagai berikut:


- Base Image: Pertama, Dockerfile dimulai dengan  FROM debian:bullseye-slim. 


- Install Dependensi: Selanjutnya,RUN apt-get update && apt-get install -y git neovim netcat python3-pip  untuk mengupdate paket dan menginstal dependensi yang diperlukan dalam kontainer. Dependensi yang diinstal termasuk git, neovim, netcat, dan python3-pip.


- Copy File Aplikasi: Pernyataan COPY hadits-d.py /home/hadits-digital/hadits-d.py dan COPY requirements.txt /home/hadits-digital/requirements.txt digunakan untuk menyalin file aplikasi warung.py dan file requirements.txt ke dalam kontainer. File aplikasi dan file dependensi ini akan digunakan dalam langkah-langkah selanjutnya.


- Install Dependensi Python: RUN pip3 install --no-cache-dir -r /home/warung.py/requirements.txt digunakan untuk menginstal dependensi Python yang diperlukan dalam aplikasi. File requirements.txt berisi daftar dependensi Python yang dibutuhkan oleh aplikasi.


- Set Working Directory: Pernyataan WORKDIR /home/hadits-digital menetapkan direktori kerja di dalam kontainer sebagai /home/warung.py. Ini adalah direktori di mana file-file aplikasi dan dependensi akan berada.


- Expose Port: Pernyataan EXPOSE 25026 digunakan untuk mengekspos port 25026 di dalam kontainer. Port ini akan digunakan untuk mengakses aplikasi Thrifting Pakaian yang berjalan dalam kontainer.


- Start Web Service: Pernyataan CMD ["streamlit", "run", "--server.port=25026", "warung.py"] digunakan untuk menjalankan perintah saat kontainer dimulai. Pada kasus ini, perintah streamlit run --server.port=25026 warung.py akan dijalankan untuk menjalankan aplikasi Streamlit.

Setelah Dockerfile selesai, Anda dapat menggunakan perintah docker build untuk membangun Docker Image dari Dockerfile tersebut. Contoh perintahnya adalah:

# 3. Mampu mendemonstrasikan pembuatan web page / web service sederhana berdasarkan permasalahan dunia nyata yang dijalankan di dalam Docker Container.

- Langkah-langkah:

Buat direktori baru untuk proyek dan masuk ke dalamnya.
Buat file Dockerfile untuk mendefinisikan Docker image yang akan digunakan.
Buat file requirements.txt untuk mengelola dependensi Python yang diperlukan.
Buat file warung-bayu yang akan berisi kode aplikasi web.


Konfigurasi Docker:

Membuat file Dockerfile:

```
FROM debian:bullseye-slim

#install
RUN apt-get update && apt-get install -y git neovim netcat python3-pip

#Copy web service files
COPY warung.py /home/WarungEl/warung.py
COPY requirements.txt /home/WarungEl/requirements.txt

#install python
RUN pip3 install --no-cache-dir -r /home/WarungEl/requirements.txt

#set working directory
WORKDIR /home/WarungEl

#expose port
EXPOSE 25026

#start web service
CMD ["streamlit", "run", "--server.port=25026", "warung.py"]
```
- Membuat file warung.py :

```
import streamlit as st

# Daftar barang dan harganya
daftarBarang = [
    {"Nama Barang": "Kopi", "Harga": 5000},
    {"Nama Barang": "Roti", "Harga": 3000},
    {"Nama Barang": "Minyak Goreng", "Harga": 10000},
    {"Nama Barang": "Sabun Mandi", "Harga": 8000},
    {"Nama Barang": "Gula", "Harga": 6000}
]

# Fungsi untuk menampilkan daftar barang
def tampilkanDaftarBarang(daftarBarang):
    st.write("Daftar Barang:")
    st.write("================")
    for barang in daftarBarang:
        st.write("Nama Barang:", barang["Nama Barang"])
        st.write("Harga: Rp{:,.0f}".format(barang["Harga"]))
        st.write("-----------------")

# Fungsi untuk mencari barang berdasarkan nama
def cariBarang(daftarBarang, namaBarang):
    for barang in daftarBarang:
        if barang["Nama Barang"].lower() == namaBarang.lower():
            return barang
    return None

# Fungsi untuk melakukan pembelian
def beliBarang(barang, uang):
    if uang >= barang["Harga"]:
        kembalian = uang - barang["Harga"]
        st.write("Pembelian berhasil!")
        st.write("Kembalian Anda: Rp{:,.0f}".format(kembalian))
    else:
        st.write("Maaf, uang Anda tidak cukup untuk membeli barang ini.")

# Fungsi untuk menambahkan uang
def tambahUang(uang):
    tambahan = st.number_input("Masukkan jumlah uang yang ingin ditambahkan:", min_value=0, step=1000)
    uang += tambahan
    st.write("Uang berhasil ditambahkan.")
    st.write("Total uang Anda sekarang: Rp{:,.0f}".format(uang))
    return uang

# Tampilan awal
tampilkanDaftarBarang(daftarBarang)

# Main program
uang = st.sidebar.number_input("Masukkan jumlah uang Anda:", min_value=0, step=1000)
nama_barang = st.sidebar.text_input("Masukkan nama barang yang ingin Anda beli:")

if st.sidebar.button("Beli"):
    barang = cariBarang(daftarBarang, nama_barang)
    if barang is not None:
        st.write("Barang ditemukan!")
        st.write("Nama Barang:", barang["Nama Barang"])
        st.write("Harga: Rp{:,.0f}".format(barang["Harga"]))
        if uang >= barang["Harga"]:
            beliBarang(barang, uang)
        else:
            st.write("Maaf, uang Anda tidak cukup untuk membeli barang ini.")
            pilihan = st.sidebar.radio("Pilihan selanjutnya:", ("Tambah Uang", "Batal"))
            if pilihan == "Tambah Uang":
                uang = tambahUang(uang)
            else:
                st.write("Terima kasih!")
```

# 4. Mampu mendemonstrasikan penggunaan Docker Compose pada port tertentu sebagai bentuk kontainerisasi program dan sistem operasi yang menjadi dasar distributed computing.

- Buat direktori baru untuk proyek dan masuk ke dalamnya.
- Buat file docker-compose.yml untuk mendefinisikan layanan yang akan dijalankan.

Konfigurasi Docker Compose:
- Membuat file docker-compose.yml:

```
version: "3"
services:
  warung-bayu:
    image: ranggaseptian/warung-bayu:1.0
    ports:
              - "25026:25026"
    networks :
              - niat-yang-suci
    volumes :
              - /home/praktikumc/1217050026/warung-bayu:/warung-bayu

networks:
    niat-yang-suci:
      external : true
```
# 5. Mampu menjelaskan project yang dibuat dalam bentuk file README.md


1. Deskripsi project
Program tersebut adalah sebuah aplikasi sederhana yang menggunakan framework Streamlit dan library Pandas di Python untuk membuat tampilan interaktif yang memungkinkan pengguna untuk mencari dan menampilkan jenis makanan yang akan di beli.

- Import Library


`import streamlit as st`

Kode di atas mengimpor  library yang digunakan dalam program ini, yaitu streamlit.streamlit adalah library yang digunakan untuk membuat antarmuka web.

- Daftar Barang (Kebutuhan Pangan) dan Juga Harganya 

```
# Daftar barang dan harganya
daftarBarang = [
    {"Nama Barang": "Kopi", "Harga": 5000},
    {"Nama Barang": "Roti", "Harga": 3000},
    {"Nama Barang": "Minyak Goreng", "Harga": 10000},
    {"Nama Barang": "Sabun Mandi", "Harga": 8000},
    {"Nama Barang": "Gula", "Harga": 6000}
]
```
Variabel daftarBarang adalah daftar barang kebutuhan pangan yang tersedia. Setiap barang direpresentasikan sebagai sebuah dictionary dengan kunci "Nama Barang" beserta "Harga" yang telah tercantum.

- Fungsi `tampilkanDaftarBarang`():

```
# Fungsi untuk menampilkan daftar barang
def tampilkanDaftarBarang(daftarBarang):
    st.write("Daftar Barang:")
    st.write("================")
    for barang in daftarBarang:
        st.write("Nama Barang:", barang["Nama Barang"])
        st.write("Harga: Rp{:,.0f}".format(barang["Harga"]))
        st.write("-----------------")
```

Fungsi ini digunakan untuk menampilkan daftar barang kebutuhan pangan ke antarmuka web (Interface) menggunakan streamlit. Setiap barang ditampilkan dengan nama barang dan harganya.

- Fungsi untuk mencari barang berdasarkan nama

```
def cariBarang(daftarBarang, namaBarang):
    for barang in daftarBarang:
        if barang["Nama Barang"].lower() == namaBarang.lower():
            return barang
    return None
```
Fungsi ini digunakan untuk menampilkan daftar barang kebutuhan pangan ke antarmuka web dengan cara mencari barang bedasarkan nama.

- Fungsi untuk melakukan pembelian

```
def beliBarang(barang, uang):
    if uang >= barang["Harga"]:
        kembalian = uang - barang["Harga"]
        st.write("Pembelian berhasil!")
        st.write("Kembalian Anda: Rp{:,.0f}".format(kembalian))
    else:
       st.write("Maaf, uang Anda tidak cukup untuk membeli barang ini.")
```
Fungsi ini digunakan untuk melakukan pembelian dari daftar barang.

- Fungsi untuk menambahkan uang

```
def tambahUang(uang):
    tambahan = st.number_input("Masukkan jumlah uang yang ingin ditambahkan:", min_value=0, step=1000)
    uang += tambahan
    st.write("Uang berhasil ditambahkan.")
    st.write("Total uang Anda sekarang: Rp{:,.0f}".format(uang))
    return uang
```
Fungsi ini digunakan untuk melakukan penambahan uang agar user dapat melukan pembelian jika user tidak dapat membeli.

- Tampilan awal

`tampilkanDaftarBarang(daftarBarang)`

Baris ini menampilkan daftar barang thrift saat program dijalankan.

- Main program

```
uang = st.sidebar.number_input("Masukkan jumlah uang Anda:", min_value=0, step=1000)
nama_barang = st.sidebar.text_input("Masukkan nama barang yang ingin Anda beli:")

if st.sidebar.button("Beli"):
    barang = cariBarang(daftarBarang, nama_barang)
    if barang is not None:
        st.write("Barang ditemukan!")
        st.write("Nama Barang:", barang["Nama Barang"])
        st.write("Harga: Rp{:,.0f}".format(barang["Harga"]))
        if uang >= barang["Harga"]:
            beliBarang(barang, uang)
        else:
            st.write("Maaf, uang Anda tidak cukup untuk membeli barang ini.")
            pilihan = st.sidebar.radio("Pilihan selanjutnya:", ("Tambah Uang", "Batal"))
            if pilihan == "Tambah Uang":
                uang = tambahUang(uang)
            else:
                st.write("Terima kasih!")
```
Variabel uang digunakan untuk melacak jumlah uang yang dimiliki oleh pengguna. Variabel menu adalah pilihan menu yang dipilih oleh pengguna melalui sidebar streamlit.
Selanjutnya, program memeriksa pilihan menu yang dipilih dan menjalankan fungsi yang sesuai:

- Jika menu "Tambah Uang" dipilih, fungsi tambahUang() akan dijalankan.

- Jika menu "Cari Barang" dipilih, pengguna diminta memasukkan nama barang yang ingin dicari melalui kotak teks. Jika nama barang valid, fungsi cariBarang() akan dijalankan untuk mencari barang tersebut.

- Jika menu "Panggil API" dipilih, fungsi panggilAPI() akan dijalankan untuk memanggil API.

- Jika menu "Selesai" dipilih, program akan menampilkan pesan penutup.

Setelah menjalankan fungsi yang sesuai, program akan terus berjalan dan menunggu interaksi pengguna melalui antarmuka web yang dihasilkan oleh streamlit.


- penjelasan bagaimana sistem operasi digunakan dalam proses containerization

Dalam proses containerization, sistem operasi digunakan untuk:

Isolasi sumber daya antara container.
Pengaturan jaringan untuk container.
Manajemen sumber daya seperti CPU dan memori.
Pemutakhiran dan perbaikan sistem operasi.
Interaksi dengan container runtime seperti Docker atau Kubernetes.

Sistem operasi yang umum digunakan dalam containerization adalah Linux dengan kernel yang mendukung fitur-fitur container. Ada juga sistem operasi khusus untuk kontainer seperti CoreOS atau RancherOS.

- Penjelasan bagaimana containerization dapat membantu mempermudah pengembangan aplikasi

Containerization mempermudah pengembangan aplikasi dengan cara-cara berikut:

Lingkungan yang konsisten di seluruh siklus pengembangan.
Portabilitas aplikasi di berbagai lingkungan.
Skalabilitas yang mudah untuk menangani permintaan yang meningkat.
Isolasi dan keamanan aplikasi yang lebih baik.
Proses pengiriman yang cepat dan efisien.

Dengan menggunakan container, pengembang dapat menghindari masalah kompatibilitas, mempercepat proses pengiriman, meningkatkan skalabilitas, dan memastikan lingkungan yang konsisten.

- Penjelasan apa itu DevOps, bagaimana DevOps membantu pengembangan aplikasi

DevOps adalah pendekatan dalam pengembangan perangkat lunak yang menggabungkan praktik-praktik pengembangan (Development) dan operasional (Operations) secara terintegrasi. DevOps bertujuan untuk menciptakan alur kerja yang efisien, berkelanjutan, dan berkolaborasi antara tim pengembangan dan tim operasional.
DevOps membantu pengembangan aplikasi dengan beberapa cara:


Kolaborasi Tim yang Kuat: DevOps mendorong kolaborasi yang erat antara tim pengembangan dan tim operasional. Ini memungkinkan komunikasi yang lebih baik, pemahaman yang lebih mendalam tentang kebutuhan bisnis, dan koordinasi yang efisien dalam menghadapi tantangan dan perubahan dalam pengembangan aplikasi. Kolaborasi yang kuat memungkinkan pengembang untuk memahami persyaratan operasional dan memastikan bahwa aplikasi dapat diimplementasikan dan dikelola dengan lancar.


Automasi Proses dan Pengujian: DevOps menganjurkan otomatisasi dalam berbagai aspek pengembangan aplikasi. Automasi dapat diterapkan dalam tahap pembangunan, pengujian, pengiriman, dan pemeliharaan aplikasi. Dengan otomatisasi, tugas-tugas berulang dapat diatasi dengan cepat, memungkinkan pengembang untuk fokus pada inovasi dan pengembangan fitur baru. Automasi juga membantu memastikan kualitas aplikasi melalui pengujian otomatis yang terintegrasi.


Penerapan Continuous Integration/Continuous Deployment (CI/CD): DevOps mendorong penerapan CI/CD, yaitu pendekatan di mana perubahan kode secara terus-menerus diuji, diintegrasikan, dan diimplementasikan ke lingkungan produksi secara otomatis. CI/CD memungkinkan pengembang untuk dengan cepat menerapkan perubahan, mengurangi waktu penyebaran, dan mengurangi risiko kesalahan. Dengan adopsi CI/CD, pengembangan aplikasi menjadi lebih responsif dan dapat memenuhi kebutuhan pasar yang berubah dengan cepat.


Monitoring dan Tindak Lanjut: DevOps menekankan pentingnya pemantauan aplikasi secara terus-menerus dan respons cepat terhadap masalah yang muncul. Dengan menggunakan alat pemantauan dan logging yang tepat, tim DevOps dapat mendapatkan wawasan tentang kinerja aplikasi dan melakukan tindakan yang diperlukan untuk mengatasi masalah sebelum berdampak pada pengguna. Dengan demikian, DevOps membantu memastikan ketersediaan dan keandalan aplikasi secara terus-menerus.


Perubahan Budaya dan Mindset: DevOps melibatkan perubahan budaya dan mindset dalam pengembangan aplikasi. Ini mencakup kolaborasi tim lintas fungsi, penerapan tanggung jawab bersama, dan adopsi siklus umpan balik yang cepat untuk terus meningkatkan proses dan produk. DevOps mendorong budaya eksperimen, di mana pengembang dapat mencoba hal-hal baru dan memperbaiki proses secara berkelanjutan.


Secara keseluruhan, DevOps membantu pengembangan aplikasi dengan mengintegrasikan tim dan alur kerja, menerapkan otomasi, mengadopsi CI/CD, meningkatkan monitoring dan tindak lanjut, serta mengubah budaya dan mindset. Dengan pendekatan ini, pengembang dapat mengurangi waktu siklus pengembangan, meningkatkan kualitas, dan memberikan aplikasi yang lebih responsif dan andal kepada pengguna.

- Berikan contoh kasus penerapan DevOps di perusahaan / industri dunia nyata (contoh bagaimana implementasi DevOps di Gojek, dsb.)

1. Gojek, perusahaan teknologi terkemuka dalam industri layanan on-demand, telah menerapkan DevOps sebagai pendekatan yang fundamental dalam pengembangan aplikasi mereka. Berikut ini adalah contoh implementasi DevOps di Gojek:


2. Automasi Proses: Gojek menggunakan alat otomasi seperti Ansible, Terraform, dan Kubernetes untuk mengotomatiskan proses pengembangan dan pengiriman aplikasi. Dengan otomasi ini, Gojek dapat dengan cepat menyediakan infrastruktur, mengatur konfigurasi, dan melakukan pengujian otomatis. Proses pengembangan dan penyebaran menjadi lebih efisien dan konsisten, memungkinkan tim untuk lebih fokus pada inovasi dan pengembangan fitur.


3. Continuous Integration/Continuous Deployment (CI/CD): Gojek menerapkan CI/CD sebagai bagian dari alur kerja DevOps. Mereka menggunakan alat seperti Jenkins dan GitLab CI/CD untuk mengotomatiskan pengujian dan penyebaran aplikasi. Setiap kali ada perubahan kode yang diajukan, sistem CI/CD akan memicu serangkaian pengujian otomatis untuk memastikan kualitas kode. Jika pengujian berhasil, perubahan kode akan secara otomatis diterapkan ke lingkungan produksi. Pendekatan ini memungkinkan Gojek untuk menerapkan perubahan dengan cepat dan responsif, sehingga mengurangi waktu penyebaran dan meningkatkan keandalan aplikasi.


4. Kolaborasi Tim: Gojek mendorong kolaborasi yang erat antara tim pengembangan, tim operasional, dan tim keamanan. Mereka memastikan komunikasi yang terbuka dan saling mendukung antara tim-tim ini. Kolaborasi yang kuat memungkinkan pengembang untuk lebih memahami kebutuhan operasional, sementara tim operasional dapat memberikan masukan yang berharga dalam perancangan dan pengiriman aplikasi. Kebersamaan tim juga membantu mempercepat perbaikan masalah dan mengoptimalkan alur kerja.


5. Monitoring dan Pengelolaan Keandalan: Gojek sangat memperhatikan monitoring aplikasi dan pengelolaan keandalan layanan mereka. Mereka menggunakan berbagai alat seperti Grafana, Prometheus, dan ELK Stack untuk memantau kinerja aplikasi dan mendeteksi masalah dengan cepat. Tim operasional dapat merespons dengan cepat terhadap gangguan layanan, memperbaiki masalah, dan memastikan tingkat keandalan yang tinggi. Monitoring secara berkelanjutan juga membantu Gojek dalam meningkatkan kinerja aplikasi dan merespons perubahan yang diperlukan dengan cepat.


Adopsi Teknologi Cloud Native: Gojek telah mengadopsi teknologi cloud native seperti Kubernetes dan Docker untuk mengelola aplikasi mereka secara efisien. Dengan menggunakan teknologi ini, Gojek dapat mengelola skala aplikasi yang besar, memastikan elastisitas, dan dengan cepat melakukan penyebaran aplikasi ke berbagai lingkungan. Pendekatan cloud native juga memungkinkan Gojek untuk memanfaatkan fleksibilitas dan skalabilitas yang ditawarkan oleh platform cloud seperti AWS, GCP, dan Azure.


Melalui implementasi DevOps yang kokoh, Gojek telah berhasil meningkatkan efisiensi pengembangan aplikasi mereka, mempercepat waktu penyebaran, meningkatkan keandalan layanan, dan menghadapi persaingan bisnis yang ketat. Penerapan DevOps membantu Gojek untuk tetap responsif terhadap perubahan pasar dan memberikan pengalaman pengguna yang unggul dalam lingkungan teknologi yang terus berkembang.

# 6. Mampu menjelaskan dan mendemonstrasikan web page / web service yang telah dapat diakses public dalam bentuk video Youtube.

https://youtu.be/PIxiU86aaQk

# 7. Mampu mempublikasikan Docker Image yang telah dibuat ke Docker Hub, untuk memudahkan penciptaan container sistem operasi di berbagai server pada distributed computing.

![Dokumentasi](Dokumentasi/yooo.png)

https://hub.docker.com/repository/docker/ranggaseptian/warung-bayu/general
