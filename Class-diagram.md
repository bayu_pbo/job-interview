# 1. Apa itu Class Diagram?

Diagram kelas adalah salah satu jenis diagram struktur pada UML yang menggambarkan dengan jelas struktur serta deskripsi class, atribut, metode, dan hubungan dari setiap objek. Ia bersifat statis, dalam artian diagram kelas bukan menjelaskan apa yang terjadi jika kelas-kelasnya berhubungan, melainkan menjelaskan hubungan apa yang terjadi. 

Diagram kelas ini sesuai jika diimplementasikan ke proyek yang menggunakan konsep object-oriented karena gambaran dari class diagram cukup mudah untuk digunakan.


# 2. Mengapa Class Diagram digunakan?
Karena Diagram Class memilili Fungsi:

- Dapat digunakan untuk analisis bisnis dan digunakan untuk membuat model sistem dari sisi bisnis.

- Dapat memberikan gambaran mengenai sistem atau perangkat lunak serta relasi-relasi yang terkandung di dalamnya.


# 3. Komponen Class Diagram

![Dokumentasi](Dokumentasi/komponen.jpg)

Penjelasan:

1. Komponen atas
Bagian ini berisikan nama class yang selalu diperlukan baik itu dalam pengklasifikasi atau objek.

2. Komponen tengah
Komponen ini berisikan atribut class yang digunakan untuk mendeskripsikan kualitas kelas. Ini hanya diperlukan saat mendeskripsikan instance tertentu dari class.

3. Komponen bawah
Bagian ini adalah komponen class diagram yang menyertakan operasi class yang ditampilkan dalam format daftar. Sehingga, setiap operasi mengambil barisnya sendiri.

Komponen ini juga menggambarkan bagaimana masing-masing class berinteraksi dengan data.

# 4. Hubungan Antar Class Diagram

Ada enam jenis utama hubungan antar kelas: pewarisan, realisasi/implementasi, komposisi, agregasi, asosiasi, dan ketergantungan. 

Tanda panah untuk enam hubungan adalah sebagai berikut:

![Dokumentasi](Dokumentasi/new2.png)

Pada keenam tipe relasi, struktur kode dari ketiga tipe relasi seperti komposisi, agregasi, dan asosiasi sama dengan menggunakan atribut untuk menyimpan referensi dari kelas lain. Oleh karena itu, keduanya harus dibedakan berdasarkan hubungan antar isinya.

**Warisan**

yaitu hubungan hirarkis antar class. Class dapat diturunkan dari class lain dan mewarisi semua atribut dan metoda class asalnya dan menambahkan fungsionalitas baru, sehingga ia disebut anak dari class yang diwarisinya. Kebalikan dari pewarisan adalah generalisasi. Contoh: Hubungan antara kelas "Mobil" dan "Sedan". Kelas "Sedan" mewarisi sifat dan perilaku dari kelas "Mobil". Ini berarti "Sedan" memiliki semua atribut dan metode yang dimiliki oleh "Mobil", dan juga dapat memiliki atribut dan metode tambahan yang spesifik untuk "Sedan".

**Realisasi / Implementasi**

menunjukkan implementasi sebuah kelas terhadap sebuah interface. Contoh: Hubungan antara kelas "Karyawan" dan "Gaji". Kelas "Karyawan" mengimplementasikan interface "Gaji", yang berarti "Karyawan" harus menyediakan implementasi untuk metode-metode yang didefinisikan dalam interface "Gaji".

**Relasi komposisi**

menunjukkan bahwa sebuah kelas terdiri dari bagian-bagian yang merupakan bagian integral dari kelas tersebut. Contoh: Hubungan antara kelas "Rumah" dan "Kamar". Kelas "Rumah" terdiri dari beberapa objek "Kamar". Jika "Rumah" dihapus, maka semua objek "Kamar" di dalamnya juga akan terhapus.

**Hubungan Agregasi**

dalah hubungan di antara kelas di mana satu kelas memiliki objek dari kelas lain sebagai bagian darinya. Contoh: Hubungan antara kelas "Sekolah" dan "Guru". Kelas "Sekolah" memiliki objek "Guru" sebagai bagian darinya. Namun, jika "Sekolah" dihapus, objek "Guru" masih dapat ada dan terkait dengan entitas lain.

**Hubungan Asosiasi**

Asosiasi, yaitu hubungan statis antar class. Umumnya menggambarkan class yang memiliki atribut berupa class lain, atau class yang harus mengetahui eksistensi class lain. Panah navigability menunjukkan arah query antar class. Contoh: Hubungan antara kelas "Pemesan" dan "Hotel". Kelas "Pemesan" memiliki asosiasi dengan kelas "Hotel" yang menunjukkan bahwa seorang pemesan dapat memesan kamar di sebuah hotel.

**Ketergantungan**

terjadi ketika sebuah kelas menggunakan atau bergantung pada kelas lain tanpa adanya hubungan langsung. Contoh: Hubungan antara kelas "Penulis" dan "Buku". Kelas "Buku" bergantung pada kelas "Penulis" karena setiap buku harus memiliki penulis yang mengarangnya. Jika kelas "Penulis" mengalami perubahan, hal ini juga dapat memengaruhi kelas "Buku".

# 5. Modifier

Visibilitas atribut dan operasi dapat direpresentasikan dengan cara berikut

- Publik - Anggota publik dapat dilihat dari mana saja dalam sistem. Dalam diagram kelas, itu diawali dengan simbol `'+'`.

- Pribadi - Anggota pribadi hanya dapat dilihat dari dalam kelas. Itu tidak dapat diakses dari luar kelas. Anggota pribadi diawali dengan simbol `'−'`.

- Terlindungi - Anggota yang dilindungi terlihat dari dalam kelas dan dari subkelas yang diwarisi dari kelas ini, tetapi tidak dari luar. Itu diawali dengan simbol` '#'`.

Untuk Gambaranya:
| Tanda      | Deskripsi          |
| -----------| ------------------ |
| `+`        | Modifier public    |
| `-`        | Modifier private   |
| `#`        | Modifier protected |


# 6. Notasi Hubungan Antar Class Diagram
```mermaid
classDiagram
    
    classA --|> classB : Inheritance
    classC --* classD : Composition
    classE --o classF : Aggregation
    classG --> classH : Association
    classI -- classJ : Link(Solid)
    classK ..> classL : Dependency
    classM ..|> classN : Realization
    classO .. classP : Link(Dashed)

```


# 7. Contoh Class Diagram


- Class Diagram menggunakan https://app.diagrams.net/

![Dokumentasi](Dokumentasi/USD.png)

- Class Diagram menggunakan mermaid.js

```mermaid
classDiagram
    class GoogleDrive {
        +listFiles()
        +uploadFile()
        +downloadFile()
        +shareFile()
    }

    class User {
        -name: string
        -email: string
        +login()
        +logout()
        +uploadFile()
        +downloadFile()
        +shareFile()
    }

    class File {
        -name: string
        -size: int
    }

    GoogleDrive --> User : Manages
    GoogleDrive --> File : Contains

```




# 8. Penjelasan Class Diagram


# 9. Referensi

https://www.dicoding.com/blog/memahami-class-diagram-lebih-baik/

https://blog.visual-paradigm.com/id/what-are-the-six-types-of-relationships-in-uml-class-diagrams/

https://glints.com/id/lowongan/wp-content/uploads/2020/08/visual-paradigm-logical.png
